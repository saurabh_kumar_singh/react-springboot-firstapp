import React, { Component } from 'react';
import "./Signup.css";
import axios from 'axios';

class Signup extends Component {
  register() {
    console.warn("state", this.state)
    axios.post('http://localhost:9090/user/adduser', this.state)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error)
      })


  }
  render() {
    return (
      <div class="Signup">
        <h3>User Registration Page</h3>
        <label>Username:</label>
        <input type="text" placeholder="email" name="username"
          onChange={(e) => { this.setState({ username: e.target.value }) }}
        /><br /><br />
        <label>Password:</label>
        <input type="password" placeholder="password" name="password"
          onChange={(e) => { this.setState({ password: e.target.value }) }}
        /><br /><br />
        <button id="submit" onClick={() => this.register()}>Register</button>
      </div>
    );
  }
}

export default Signup; 
