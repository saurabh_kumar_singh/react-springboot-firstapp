import React, { PureComponent } from 'react';
import Login from "./Login";
import { Link, BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Signup from "./Signup";
import "./Routes.css";

class Navbar extends React.Component {
    render() {
        return (
            <div class="container">
                <Router>
                
                    <nav>
                        <Link to="/">Login</Link>
                       
                        <Link to="/containers/Login">Signup</Link>
                    </nav>
                    
                    <Switch>
                        <Route
                            path="/"
                            component={Login}
                            exact
                        />
                        <Route
                            path="/containers/Login"
                            component={Signup}
                        />
                    </Switch>
                </Router>
            </div>
        );
    }
}

export default Navbar;
