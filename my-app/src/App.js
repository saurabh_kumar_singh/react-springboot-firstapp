import './App.css';
import Navbar from "./containers/Routes";


function App() {
  return (
    <div className="App">
      <Navbar />
    </div>
  );
}
export default App;
